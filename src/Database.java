//Stwórz aplikację, która dodaje wpisy do rejestru, a po przedawnieniu usuwa je w obiegu ktory odswieza baze danych.
//        1. Stworz klase Record ktora reprezentuje pojedynczy wpis w bazie danych. klasa powinna posiadac:
//        -  czas dodania - data lub timestamp
//        -  czas waznosci - datę przedawnienia lub czas w milisekundach po jakim rekord ulega przedawnieniu
//        - identyfikator sprawy - liczba
//        - nazwę sprawy - tekst, moze byc jedno slowo

//        2. Stworz klase Database ktora posiada mapę rekordów Record.
//        - dodaj metodę dodawania rekordów do mapy. Rekordy powinny byc ladowane z konsoli
//        - dodaj metodę szukania rekordów po ich identyfikatorze
//        - dodaj metodę refresh() która wykonuje obieg przez wszystkie rekordy bazy danych i sprawdza czy rekord
//        nie jest przedawniony. Jeśli rekord jest przedawniony powinien zostać usunięty a w konsoli powinien się
//        pojawić odpowiedni komunikat.
//        Dodaj maina, w którym możesz dodawać lub odswieżać rekordy w bazie
//
//        https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/PrzedawnioneRekordy
//          /src/com/amen/records/?at=master

import com.sun.org.apache.regexp.internal.RE;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Database {
    private Map<Integer, Record> mapa = new HashMap<>();
    private int identyfikator = 1;

    public void addRecord() {
        Scanner sc = new Scanner(System.in);

        System.out.println("podaj record");
        String line = sc.nextLine();

        System.out.println("podaj identyfikator liczbowy (domyslnie kolejny numer:wklep default)");
        String line2 = sc.nextLine();

        Record newRecord = new Record();

        newRecord.setCaseDescription(line);

        if (line2.trim().equals("default")) {
            newRecord.setCaseNumber(identyfikator);
            mapa.put(identyfikator, newRecord);
        } else {
            newRecord.setCaseNumber(Integer.parseInt(line2.trim()));
            mapa.put(Integer.parseInt(line2.trim()), newRecord);
        }
        identyfikator++;
    }

    public void searchForRecord(int id) {
        if (mapa.get(id) == null) {
            System.out.println("brak rekordu");
        } else {
            System.out.println(mapa.get(id).toString());
        }
    }

    public void refreshDB() {
        Iterator<Map.Entry<Integer, Record>> iterator = mapa.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Integer, Record> wpis = iterator.next();
            if (wpis.getValue().getDelayDate().before(Timestamp.valueOf(LocalDateTime.now()))) {
                mapa.remove(wpis.getKey());
            }
        }


    }


}
