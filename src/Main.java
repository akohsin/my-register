//Stwórz aplikację, która dodaje wpisy do rejestru, a po przedawnieniu usuwa je w obiegu ktory odswieza baze danych.
//        1. Stworz klase Record ktora reprezentuje pojedynczy wpis w bazie danych. klasa powinna posiadac:
//        -  czas dodania - data lub timestamp
//        -  czas waznosci - datę przedawnienia lub czas w milisekundach po jakim rekord ulega przedawnieniu
//        - identyfikator sprawy - liczba
//        - nazwę sprawy - tekst, moze byc jedno slowo
//        2. Stworz klase Database ktora posiada mapę rekordów Record.
//        - dodaj metodę dodawania rekordów do mapy. Rekordy powinny byc ladowane z konsoli
//        - dodaj metodę szukania rekordów po ich identyfikatorze
//        - dodaj metodę refresh() która wykonuje obieg przez wszystkie rekordy bazy danych i sprawdza czy rekord
//        nie jest przedawniony. Jeśli rekord jest przedawniony powinien zostać usunięty a w konsoli powinien się
//        pojawić odpowiedni komunikat.
//        Dodaj maina, w którym możesz dodawać lub odswieżać rekordy w bazie
//
//        https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/PrzedawnioneRekordy
//          /src/com/amen/records/?at=master


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Database bazaDanych = new Database();
        Scanner sc = new Scanner(System.in);
        boolean isRunning = true;

        do {
            System.out.println("podaj metode (refresh, dodaj record, search, exit)");
            String line = sc.nextLine();
            switch (line) {
                case "refresh": {
                    bazaDanych.refreshDB();
                    break;
                }
                case "dodaj record": {
                    bazaDanych.addRecord();
                    break;
                }
                case "search": {
                    System.out.println("podaj id");
                    int wpis = Integer.parseInt(sc.nextLine().trim());
                    bazaDanych.searchForRecord(wpis);
                    break;
                }
                case "exit": {
                    isRunning = false;
                    break;
                }
            }
        }
        while (isRunning);

    }
}
