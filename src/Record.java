import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Timer;

import static java.sql.Date.*;
//Stwórz aplikację, która dodaje wpisy do rejestru, a po przedawnieniu usuwa je w obiegu ktory odswieza baze danych.
//        1. Stworz klase Record ktora reprezentuje pojedynczy wpis w bazie danych. klasa powinna posiadac:
//        -  czas dodania - data lub timestamp
//        -  czas waznosci - datę przedawnienia lub czas w milisekundach po jakim rekord ulega przedawnieniu
//        - identyfikator sprawy - liczba
//        - nazwę sprawy - tekst, moze byc jedno slowo
//        2. Stworz klase Database ktora posiada mapę rekordów Record.
//        - dodaj metodę dodawania rekordów do mapy. Rekordy powinny byc ladowane z konsoli
//        - dodaj metodę szukania rekordów po ich identyfikatorze
//        - dodaj metodę refresh() która wykonuje obieg przez wszystkie rekordy bazy danych i sprawdza czy rekord
//        nie jest przedawniony. Jeśli rekord jest przedawniony powinien zostać usunięty a w konsoli powinien się
//        pojawić odpowiedni komunikat.
//        Dodaj maina, w którym możesz dodawać lub odswieżać rekordy w bazie
//
//        https://bitbucket.org/saintamen/itnabanksda/src/0076d23f58e93a8276f865b400c4057990e0fe30/PrzedawnioneRekordy
//          /src/com/amen/records/?at=master

public class Record {
    private Timestamp addTime;
    private int caseNumber;
    private String caseDescription;
    private Timestamp delayDate;


    public Record(Timestamp delayDate) {
        this.addTime = Timestamp.valueOf(LocalDateTime.now());
        this.delayDate = delayDate;
    }

    public Record() {
        this.addTime = Timestamp.valueOf(LocalDateTime.now());
        this.delayDate = Timestamp.valueOf(LocalDateTime.now().plusDays(30));
    }

    public int getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(int caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public Timestamp getDelayDate() {
        return delayDate;
    }

    @Override
    public String toString() {
        return "Record{" +
                "addTime=" + addTime +
                ", caseNumber=" + caseNumber +
                ", caseDescription='" + caseDescription + '\'' +
                ", delayDate=" + delayDate +
                '}';
    }
}

